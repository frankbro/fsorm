﻿module FsOrm.Tests.Util

open Microsoft.VisualStudio.TestTools.UnitTesting

//-----------------------------------------------------------------------------
let inline (=?) (a: 't when 't : equality) (b: 't when 't : equality) = 
    if a <> b then
        raise <| AssertFailedException(sprintf "A =? B is false.\nA =\n%A\nB =\n%A" a b)

//-----------------------------------------------------------------------------
