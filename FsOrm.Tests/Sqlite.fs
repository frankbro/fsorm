﻿module FsOrm.Tests.Sqlite

open System

open Microsoft.FSharp.Reflection
open Microsoft.VisualStudio.TestTools.UnitTesting

open FsOrm.Data
open FsOrm.FsOrm
open FsOrm.Query
open FsOrm.Sqlite
open FsOrm.Util

open FsOrm.Tests.Util
open FsOrm.Tests.Data

type PrimitiveTypes 
with
    static member NewBool pk value = { PrimitiveTypes.New with Id = Existing pk; Bool = value }
    static member NewByte pk value = { PrimitiveTypes.New with Id = Existing pk; Byte = value }
    static member NewSByte pk value = { PrimitiveTypes.New with Id = Existing pk; SByte = value }
    static member NewInt16 pk value = { PrimitiveTypes.New with Id = Existing pk; Int16 = value }
    static member NewUInt16 pk value = { PrimitiveTypes.New with Id = Existing pk; UInt16 = value }
    static member NewInt32 pk value = { PrimitiveTypes.New with Id = Existing pk; Int32 = value }
    static member NewUInt32 pk value = { PrimitiveTypes.New with Id = Existing pk; UInt32 = value }
    static member NewInt64 pk value = { PrimitiveTypes.New with Id = Existing pk; Int64 = value }
    static member NewUInt64 pk value = { PrimitiveTypes.New with Id = Existing pk; UInt64 = value }
    static member NewChar pk value = { PrimitiveTypes.New with Id = Existing pk; Char = value }
    static member NewString pk value = { PrimitiveTypes.New with Id = Existing pk; String = value }
    //static member NewDecimal pk value = { PrimitiveTypes.New with Id = Existing pk; Decimal = value }
    static member NewFloat32 pk value = { PrimitiveTypes.New with Id = Existing pk; Float32 = value }
    static member NewFloat pk value = { PrimitiveTypes.New with Id = Existing pk; Float = value }
    static member NewDateTime pk value = { PrimitiveTypes.New with Id = Existing pk; DateTime = value }
    static member NewGuid pk value = { PrimitiveTypes.New with Id = Existing pk; Guid = value }

(*
type [<Measure>] arrayTest

type ArrayTest = {
    Id: PrimaryKey<arrayTest>
    Numeric: int
    Text: string
    Array: string []
}
with
    static member New = {
        Id = New
        Numeric = 10
        Text = "lol"
        Array = [| "top"; "kek" |]
    }
    member x.Id.Get =
        match x.Id with
        | New -> failwith "No id"
        | Existing id -> id
        
type [<Measure>] listTest

type ListTest = {
    Id: PrimaryKey<listTest>
    Numeric: int
    Text: string
    List: string list
}
with
    static member New = {
        Id = New
        Numeric = 10
        Text = "lol"
        List = [ "top"; "kek" ]
    }
    member x.Id.Get =
        match x.Id with
        | New -> failwith "No id"
        | Existing id -> id

type [<Measure>] seqTest

type SeqTest = {
    Id: PrimaryKey<seqTest>
    Numeric: int
    Text: string
    Seq: string seq
}
with
    static member New = {
        Id = New
        Numeric = 10
        Text = "lol"
        Seq = seq { yield "top"; yield "kek" }
    }
    member x.Id.Get =
        match x.Id with
        | New -> failwith "No id"
        | Existing id -> id

type [<Measure>] simpleTupleTest

type SimpleTupleTest = {
    Id: PrimaryKey<simpleTupleTest>
    Tuple: int * string
}
with
    static member New = {
        Id = New
        Tuple = 0, "lol"
    }
    member x.Id.Get =
        match x.Id with
        | New -> failwith "No id"
        | Existing id -> id
*)

[<TestClass>]
type ``Sqlite tests`` () = 
    let store = SqliteOrm () :> IOrm

    (*
    [<TestMethod>]
    member x.``SimpleTupleTest: Save and load`` () =
        let name = "SimpleTupleTest, Save and load"
        let value = { SimpleTupleTest.New with Id = Existing 1<simpleTupleTest> }
        let database = store.OpenDatabase name
        do database.DeleteTable<SimpleTupleTest> ()
        let table = database.OpenTable<SimpleTupleTest> ()
        let value = table.Save value
        let key = value.Id.Get
        let result = table.Load key
        value =? result

    [<TestMethod>]
    member x.``InnerRecordTest: Save and load`` () =
        let name = "InnerRecordTest, Save and load"
        let value = { InnerRecordTest.New with Id = Existing 1<innerRecordTest> }
        let database = store.OpenDatabase name
        do database.DeleteTable<InnerRecordTest> ()
        let table = database.OpenTable<InnerRecordTest> ()
        let value = table.Save value
        let key = value.Id.Get
        let result = table.Load key
        value =? result

    [<TestMethod>]
    member x.``ArrayTest: Save and load`` () =
        let name = "ArrayTest, Save and load"
        let value = { ArrayTest.New with Id = Existing 1<arrayTest> }
        let database = store.OpenDatabase name
        do database.DeleteTable<ArrayTest> ()
        let table = database.OpenTable<ArrayTest> ()
        let value = table.Save value
        let key = value.Id.Get
        let result = table.Load key
        value =? result

    [<TestMethod>]
    member x.``ListTest: Save and load`` () =
        let name = "ListTest, Save and load"
        let value = { ListTest.New with Id = Existing 1<listTest> }
        let database = store.OpenDatabase name
        do database.DeleteTable<ListTest> ()
        let table = database.OpenTable<ListTest> ()
        let value = table.Save value
        let key = value.Id.Get
        let result = table.Load key
        value =? result

    [<TestMethod>]
    member x.``SeqTest: Save and load`` () =
        let name = "SeqTest, Save and load"
        let value = { SeqTest.New with Id = Existing 1<seqTest> }
        let database = store.OpenDatabase name
        do database.DeleteTable<SeqTest> ()
        let table = database.OpenTable<SeqTest> ()
        let value = table.Save value
        let key = value.Id.Get
        let result = table.Load key
        value =? result
    *)

    [<TestMethod>]
    member x.``Access test: Save and load`` () =
        let name = "Access test, Save and load"
        let value = { AccessTest.New with PrimaryKey = Existing 1<accessTest> } 
        let database = store.OpenDatabase name
        do database.DeleteTable<AccessTest> ()
        let table = database.OpenTable<accessTest, AccessTest> ()
        let key = table.Insert value
        let result = table.SelectKey key
        value =? result

    [<TestMethod>]
    member x.``Primitive types: Save and load`` () = 
        let name = "Primitive types, Save and load"
        let value = { PrimitiveTypes.New with Id = Existing 1<primitiveTypes> }
        let database = store.OpenDatabase name
        do database.DeleteTable<PrimitiveTypes> ()
        let table = database.OpenTable<primitiveTypes, PrimitiveTypes> ()
        let key = table.Insert value
        let result = table.SelectKey key
        value =? result

    [<TestMethod>]
    member x.``Primitive types: Query`` () =
        let name = "Primitive types, Query"
        let value = { PrimitiveTypes.New with Id = Existing 1<primitiveTypes> }
        let database = store.OpenDatabase name
        do database.DeleteTable<PrimitiveTypes> ()
        let table = database.OpenTable<primitiveTypes, PrimitiveTypes> ()
        let key = table.Insert value
        //
        let i = ref 2<primitiveTypes>
        let getId () =
            let value = !i
            i := value + 1<primitiveTypes>
            value
        //Bool ----------------------------------------------------------------
        let test = PrimitiveTypes.NewBool (getId ()) true
        let key = table.Insert test
        // =
        let query = <@ fun test -> test.Bool = false @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Bool <> true @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Bool > false @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Bool < true @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Bool >= false @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Bool <= true @>
        let values = table.Select query
        values =? [| value; test |]
        //Byte ----------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewByte (getId ()) 1uy
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Byte = 0uy @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Byte <> 1uy @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Byte > 0uy @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Byte < 1uy @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Byte >= 0uy @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Byte <= 1uy @>
        let values = table.Select query
        values =? [| value; test |]
        //SByte ---------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewSByte (getId ()) 1y
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.SByte = 0y @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.SByte <> 1y @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.SByte > 0y @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.SByte < 1y @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.SByte >= 0y @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.SByte <= 1y @>
        let values = table.Select query
        values =? [| value; test |]
        //Int16 ---------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewInt16 (getId ()) 1s
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Int16 = 0s @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Int16 <> 1s @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Int16 > 0s @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Int16 < 1s @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Int16 >= 0s @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Int16 <= 1s @>
        let values = table.Select query
        values =? [| value; test |]
        //UInt16 --------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewUInt16 (getId ()) 1us
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.UInt16 = 0us @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.UInt16 <> 1us @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.UInt16 > 0us @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.UInt16 < 1us @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.UInt16 >= 0us @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.UInt16 <= 1us @>
        let values = table.Select query
        values =? [| value; test |]
        //Int -----------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewInt32 (getId ()) 1
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Int32 = 0 @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Int32 <> 1 @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Int32 > 0 @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Int32 < 1 @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Int32 >= 0 @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Int32 <= 1 @>
        let values = table.Select query
        values =? [| value; test |]
        //UInt32 --------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewUInt32 (getId ()) 1u
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.UInt32 = 0u @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.UInt32 <> 1u @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.UInt32 > 0u @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.UInt32 < 1u @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.UInt32 >= 0u @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.UInt32 <= 1u @>
        let values = table.Select query
        values =? [| value; test |]
        //Int64 ---------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewInt64 (getId ()) 1L
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Int64 = 0L @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Int64 <> 1L @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Int64 > 0L @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Int64 < 1L @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Int64 >= 0L @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Int64 <= 1L @>
        let values = table.Select query
        values =? [| value; test |]
        //UInt64 --------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewUInt64 (getId ()) 1uL
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.UInt64 = 0uL @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.UInt64 <> 1uL @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.UInt64 > 0uL @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.UInt64 < 1uL @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.UInt64 >= 0uL @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.UInt64 <= 1uL @>
        let values = table.Select query
        values =? [| value; test |]
        //Char ----------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewChar (getId ()) '1'
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Char = '0' @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Char <> '1' @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Char > '0' @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Char < '1' @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Char >= '0' @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Char <= '1' @>
        let values = table.Select query
        values =? [| value; test |]
        //String --------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewString (getId ()) "1"
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.String = "0" @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.String <> "1" @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.String > "0" @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.String < "1" @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.String >= "0" @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.String <= "1" @>
        let values = table.Select query
        values =? [| value; test |]
        //Decimal -------------------------------------------------------------
        (*
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewDecimal (getId ()) 1M
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Decimal = 0M @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Decimal <> 1M @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Decimal > 0M @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Decimal < 1M @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Decimal >= 0M @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Decimal <= 1M @>
        let values = table.Select query
        values =? [| value; test |]
        *)
        //Float32 -------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewFloat32 (getId ()) 1.f
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Float32 = 0.f @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Float32 <> 1.f @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Float32 > 0.f @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Float32 < 1.f @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Float32 >= 0.f @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Float32 <= 1.f @>
        let values = table.Select query
        values =? [| value; test |]
        //Float ---------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let test = PrimitiveTypes.NewFloat (getId ()) 1.
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Float = 0. @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Float <> 1. @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Float > 0. @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Float < 1. @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Float >= 0. @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Float <= 1. @>
        let values = table.Select query
        values =? [| value; test |]
        ()
        //DateTime ------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let min = DateTime.MinValue
        let max = DateTime.MaxValue
        let test = PrimitiveTypes.NewDateTime (getId ()) max
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.DateTime = min @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.DateTime <> max @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.DateTime > min @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.DateTime < max @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.DateTime >= min @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.DateTime <= max @>
        let values = table.Select query
        values =? [| value; test |]
        ()
        //Guid ----------------------------------------------------------------
        do table.DeleteKey test.Id.Get
        let guid = Guid.NewGuid ()
        let empty = Guid.Empty
        let test = PrimitiveTypes.NewGuid (getId ()) guid
        let _ = table.Insert test
        // =
        let query = <@ fun test -> test.Guid = empty @>
        let values = table.Select query
        values =? [| value |]
        // <>
        let query = <@ fun test -> test.Guid <> guid @>
        let values = table.Select query
        values =? [| value |]
        // >
        let query = <@ fun test -> test.Guid > empty @>
        let values = table.Select query
        values =? [| test |]
        // <
        let query = <@ fun test -> test.Guid < guid @>
        let values = table.Select query
        values =? [| value |]
        // >=
        let query = <@ fun test -> test.Guid >= empty @>
        let values = table.Select query
        values =? [| value; test |]
        // <=
        let query = <@ fun test -> test.Guid <= guid @>
        let values = table.Select query
        values =? [| value; test |]
        ()

    (*
    [<TestMethod>]
    member x.``Union type: Save and load`` () =
        let name = "Union type, Save and load"

        let database = store.OpenDatabase name
        do database.DeleteTable<UnionType> ()
        let table = database.OpenTable<UnionType> ()

        (*
        let value = { UnionType.NewB with Id = Existing 1<unionType> }
        let value = table.Save value
        let key = value.Id.Get
        let result = table.Load key
        value =? result
        *)

        let value = { UnionType.NewC with Id = Existing 2<unionType> }
        let value = table.Save value
        let key = value.Id.Get
        let result = table.Load key
        value =? result

        let value = { UnionType.NewD with Id = Existing 3<unionType> }
        let value = table.Save value
        let key = value.Id.Get
        let result = table.Load key
        value =? result

    [<TestMethod>]
    member x.``Record type`` () =
        let value = RecordType.New
        let t = value.GetType ()
        let table = getTable t
        let expectedTable = RecordType.ExpectedTable
        table =? Success expectedTable

    [<TestMethod>]
    member x.``Tuple type`` () =
        let value = TupleTypes.New
        let t = value.GetType ()
        let table = getTable t
        let expectedTable = TupleTypes.ExpectedTable
        table =? Success expectedTable

    [<TestMethod>]
    member x.``Generic types`` () =
        let value = GenericTypes.New
        let t = value.GetType ()
        let table = getTable t
        let expectedTable = GenericTypes.ExpectedTable
        table =? Success expectedTable
    *)

    (*
    [<TestMethod>]
    member x.``Foreign key type`` () =
        let value = ForeignKeyType.New
        let t = value.GetType ()
        let table = getTable t
        match table with
        | Failure s ->
            raise <| AssertFailedException(s)
        | Success table ->
            let expectedTable = ForeignKeyType.ExpectedTable
            table =? expectedTable
            let foreignKeyTables = table.getForeignKeyTables ()
            foreignKeyTables =? Success [ NullableType.ExpectedTable ]

    [<TestMethod>]
    member x.``Nullable key type`` () =
        let value = NullableKeyType.New
        let t = value.GetType ()
        let table = getTable t
        match table with
        | Failure s ->
            raise <| AssertFailedException(s)
        | Success table ->
            let expectedTable = NullableKeyType.ExpectedTable
            table =? expectedTable
            let foreignKeyTables = table.getForeignKeyTables ()
            foreignKeyTables =? Success [ NullableType.ExpectedTable ]
    *)
        