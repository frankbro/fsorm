﻿module FsOrm.Tests.Query

open System

open Microsoft.VisualStudio.TestTools.UnitTesting

open FsOrm.FsOrm
open FsOrm.Query

open FsOrm.Tests.Util

type [<Measure>] testId
type [<Measure>] foreignId

type TestUnion = 
    | A 
    | B of int

type TestRecord = {
    A: int
    B: string
}

type Test = {
    Id: PrimaryKey<testId>
    Foreign: ForeignKey<foreignId>
    Numeric: int
    Text: string
    DateTime: DateTime
    Guid: Guid
    Union: TestUnion
    Record: TestRecord
    Tuple: int * string
}
with
    static member New = {
        Id = Existing 0<testId>
        Foreign = ForeignKey 0<foreignId>
        Numeric = 0
        Text = ""
        DateTime = DateTime.MinValue
        Guid = Guid.Empty
        Union = A
        Record = { A = 0; B = "" }
        Tuple = 0, ""
    }

type TestHolder = {
    Test: Test
}
with
    static member New = {
        Test = Test.New
    }

let getNumeric (test: Test) = test.Numeric

[<TestClass>]
type ``Query tests`` () = 

    [<TestMethod>]
    member __.``Numeric`` () = 
        let expr = parse <@ fun test -> test.Numeric = 3 @>
        expr =? "Numeric = 3"

    [<TestMethod>]
    member __.``Text`` () =
        let expr = parse <@ fun test -> test.Text = "lol" @>
        expr =? "Text = 'lol'"

    [<TestMethod>]
    member __.``Equal`` () =
        let expr = parse <@ fun test -> test.Numeric = 3 @>
        expr =? "Numeric = 3"

    [<TestMethod>]
    member __.``NotEqual`` () =
        let expr = parse <@ fun test -> test.Numeric <> 3 @>
        expr =? "Numeric <> 3"

    [<TestMethod>]
    member __.``GreaterThan`` () =
        let expr = parse <@ fun test -> test.Numeric > 3 @>
        expr =? "Numeric > 3"

    [<TestMethod>]
    member __.``LesserThan`` () =
        let expr = parse <@ fun test -> test.Numeric < 3 @>
        expr =? "Numeric < 3"

    [<TestMethod>]
    member __.``GreaterOrEqualTo`` () =
        let expr = parse <@ fun test -> test.Numeric >= 3 @>
        expr =? "Numeric >= 3"

    [<TestMethod>]
    member __.``LesserOrEqualTo`` () =
        let expr = parse <@ fun test -> test.Numeric <= 3 @>
        expr =? "Numeric <= 3"

    [<TestMethod>]
    member __.``And`` () =
        let expr = parse <@ fun test -> test.Numeric < 3 && test.Numeric > 5 @>
        expr =? "Numeric < 3 AND Numeric > 5"

    [<TestMethod>]
    member __.``Or`` () =
        let expr = parse <@ fun test -> test.Numeric < 3 || test.Numeric > 5 @>
        expr =? "Numeric < 3 OR Numeric > 5"

    [<TestMethod>]
    member __.``Get value from variable`` () =
        let value = 3
        let expr = parse <@ fun test -> test.Numeric = value @>
        expr =? "Numeric = 3"
        
    [<TestMethod>]
    member __.``DateTime and Guid constants`` () =
        let expr1 = parse <@ fun test -> test.DateTime = DateTime.MinValue @>
        expr1 =? (sprintf "DateTime = %d" (DateTime.MinValue.Ticks))
        let expr2 = parse <@ fun test -> test.Guid = Guid.Empty @>
        expr2 =? (sprintf "Guid = '%s'" (string Guid.Empty))

    [<TestMethod>]
    member __.``DateTime now and utcnow resolves`` () =
        let expr1 = parse <@ fun test -> test.DateTime = DateTime.Now @>
        let now = DateTime.Now
        let result1 = expr1
        let extracted1 = result1.Replace ("DateTime = ", "")
        let value1 = Convert.ToInt64 extracted1
        value1 / 10000000L =? now.Ticks / 10000000L
        let expr2 = parse <@ fun test -> test.DateTime = DateTime.UtcNow @>
        let now = DateTime.UtcNow
        let result2 = expr2
        let extracted2 = result2.Replace ("DateTime = ", "")
        let value2 = Convert.ToInt64 extracted2
        value2 / 10000000L =? now.Ticks / 10000000L

    [<TestMethod>]
    member __.``Between`` () =
        let expr = parse <@ fun test -> between test.Numeric 0 5 @>
        expr =? "Numeric BETWEEN 0 AND 5"

    [<TestMethod>]
    member __.``In`` () =
        let expr = parse <@ fun test -> isIn test.Numeric [|0; 1; 2|] @>
        expr =? "Numeric IN (0, 1, 2)"

    [<TestMethod>]
    member __.``Not`` () =
        let expr = parse <@ fun test -> not (test.Numeric = 0) @>
        expr =? "NOT Numeric = 0"

    [<TestMethod>]
    member __.``Like`` () =
        let expr = parse <@ fun test -> like test.Text "%lol%" @>
        expr =? "Text LIKE '%lol%'"

    [<TestMethod>]
    member __.``Can fetch properties`` () =
        let value = Test.New
        let expr = parse <@ fun test -> test.Numeric = value.Numeric @>
        expr =? "Numeric = 0"

    [<TestMethod>]
    member __.``Can fetch nested properties`` () =
        let value = TestHolder.New
        let expr = parse <@ fun test -> test.Numeric = value.Test.Numeric @>
        expr =? "Numeric = 0"

    [<TestMethod>]
    member __.``Can call functions`` () =
        let value = Test.New
        let expr = parse <@ fun test -> test.Numeric = getNumeric value @>
        expr =? "Numeric = 0"

    [<TestMethod>]
    member __.``Can call lambdas`` () =
        raise <| AssertFailedException()

    [<TestMethod>]
    member __.``Support option`` () =
        raise <| AssertFailedException()

    [<TestMethod>]
    member __.``Can do record field equality`` () =
        let expr = parse <@ fun test -> test.Record.A = 0 @>
        expr =? "Record_A = 0"

    [<TestMethod>]
    member __.``Can do record equality`` () =
        let expr = parse <@ fun test -> test.Record = { A = 0; B = "kek" } @>
        expr =? "Record_A = 0 && Record_B = 'kek'"
        let expr = parse <@ fun test -> { A = 0; B = "kek" } = test.Record @>
        expr =? "Record_A = 0 && Record_B = 'kek'"
        // Fucked up special case
        let expr = parse <@ fun test -> test.Record = { B = "kek"; A = 0 } @>
        expr =? "Record_A = 0 && Record_B = 'kek'"

    [<TestMethod>]
    member __.``Support record with notation`` () =
        let value = { A = 0; B = "" }
        let expr = parse <@ fun test -> test.Record = { value with B = "kek" } @>
        expr =? "Record_A = 0 && Record_B = 'kek'"

    [<TestMethod>]
    member __.``Can do union equality`` () =
        let expr = parse <@ fun test -> test.Union = B 0 @>
        expr =? "Union_Tag = 1 && Union_B_0 = 0"

    [<TestMethod>]
    member __.``Can do tuple equality`` () =
        let expr = parse <@ fun test -> test.Tuple = (0, "kek") @>
        expr =? "Tuple_0 = 0 && Tuple_1 = 'kek'"

    [<TestMethod>]
    member __.``We can do pattern matching on values`` () =
        let expr = 
            parse <@ fun test ->
                match test.Numeric with
                | 0 -> true
                | _ -> false
            @>
        expr =? "Numeric = 0"

    [<TestMethod>]
    member __.``We can do pattern matching on records`` () =
        let expr = 
            parse <@ fun test ->
                match test with
                | { Numeric = 0 } -> true
                | _ -> false
            @>
        expr =? "Numeric = 0"

    [<TestMethod>]
    member __.``We can do pattern matching on unions`` () =
        let expr = 
            parse <@ fun test ->
                match test.Union with
                | B 0 -> true
                | _ -> false
            @>
        expr =? "Union_Tag = 1 && Union_B_0 = 0"

    [<TestMethod>]
    member __.``We can do pattern matching on partial unions`` () =
        let expr = 
            parse <@ fun test ->
                match test.Union with
                | B _ -> true
                | _ -> false
            @>
        expr =? "Union_Tag = 1"

    [<TestMethod>]
    member __.``We can do pattern matching on partial tuples`` () =
        let expr = 
            parse <@ fun test ->
                match test.Tuple with
                | (0, _) -> true
                | (_, "kek") -> true
                | _ -> false
            @>
        expr =? "(Tuple_0 = 0) || (Tuple_1 = 'kek'"

    [<TestMethod>]
    member __.``We can do pattern matching on multiple values`` () =
        let expr = 
            parse <@ fun test ->
                match test.Numeric with
                | 0 -> true
                | 1 -> false
                | 2 -> true
                | _ -> false
            @>
        expr =? "(Numeric = 0 || Numeric = 2) && Numeric <> 1"

    [<TestMethod>]
    member __.``We can do reversed pattern matching`` () =
        let expr = 
            parse <@ fun test ->
                match test.Numeric with
                | 0 -> false
                | _ -> true
            @>
        expr =? "Numeric <> 0"

    [<TestMethod>]
    member __.``We can do if else`` () =
        let expr = parse <@ fun test -> test.Numeric = if true then 0 else 1 @>
        expr =? "Numeric = 0"