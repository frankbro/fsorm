﻿module FsOrm.Tests.Data

open System

open Microsoft.FSharp.Reflection
open Microsoft.VisualStudio.TestTools.UnitTesting

open FsOrm.Data
open FsOrm.FsOrm
open FsOrm.Value
open FsOrm.Util

open FsOrm.Tests.Util

// ----------------------------------------------------------------------------

let primaryKey container = PrimaryKey container
let primitive container primitive = Primitive (primitive, container)
let record t container values = Record (t, values, container)
let union t container values = Union (t, values, container)
let tuple t container values = Tuple (t, values, container)

let recordContainer t container name index = RecordContainer { Container = container; RecordType = t; FieldName = name; FieldIndex = index }
let unionContainer t container tagName tagIndex argName argIndex = UnionContainer { Container = container; UnionType = t; UnionTagName = tagName; UnionTagIndex = tagIndex; ArgName = argName; ArgIndex = argIndex }
let tupleContainer index container = TupleContainer { Container = container; Index = index }

// ----------------------------------------------------------------------------

type [<Measure>] primitiveTypes

type PrimitiveTypes = {
    Id: PrimaryKey<primitiveTypes>
    Bool: bool
    Byte: byte
    SByte: sbyte
    Int16: int16
    UInt16: uint16
    Int32: int
    UInt32: uint32
    Int64: int64
    UInt64: uint64
    Char: char
    String: string
    //Decimal: decimal
    Float32: float32
    Float: float
    DateTime: DateTime
    Guid: Guid
}
with
    static member New = 
        { 
            Id = New
            Bool = false
            Byte = 0uy
            SByte = 0y
            Int16 = 0s
            UInt16 = 0us
            Int32 = 0
            UInt32 = 0u
            Int64 = 0L
            UInt64 = 0UL
            Char = '0'
            String = "0"
            //Decimal = 0.m
            Float32 = 0.f
            Float = 0.
            DateTime = DateTime.MinValue
            Guid = Guid.Empty
        }
    static member ExpectedTable = 
        let rc = recordContainer typeof<PrimitiveTypes> Root
        let values = 
            [|
                primaryKey (rc "Id" 0)
                primitive (rc "Bool" 1) Primitive.Boolean 
                primitive (rc "Byte" 2) Primitive.Byte 
                primitive (rc "SByte" 3) Primitive.SByte 
                primitive (rc "Int16" 4) Primitive.Int16 
                primitive (rc "UInt16" 5) Primitive.UInt16 
                primitive (rc "Int32" 6) Primitive.Int32 
                primitive (rc "UInt32" 7) Primitive.UInt32 
                primitive (rc "Int64" 8) Primitive.Int64 
                primitive (rc "UInt64" 9) Primitive.UInt64 
                primitive (rc "Char" 10) Primitive.Char 
                primitive (rc "String" 11) Primitive.String 
                primitive (rc "Float32" 12) Primitive.Float32 
                primitive (rc "Float" 13) Primitive.Float
                primitive (rc "DateTime" 14) Primitive.DateTime
                primitive (rc "Guid" 15) Primitive.Guid
            |]
        let value = Record (typeof<PrimitiveTypes>, values, Root)
        value

// ----------------------------------------------------------------------------

type [<Measure>] unionType 

type A =
    | B
    | C of string
    | D of int * string

type UnionType = {
    Id: PrimaryKey<unionType>
    Union: A
}
with
    static member NewB = { Id = New; Union = B }
    static member NewC = { Id = New; Union = C "top" }
    static member NewD = { Id = New; Union = D (0, "kek") }
    static member ExpectedTable =
        let rc = recordContainer typeof<UnionType> Root
        let uc = unionContainer typeof<A> (rc "Union" 1)
        let u = union typeof<A> (rc "Union" 1)
        let unionValues =
            [|
                Tag (UnionTag (typeof<A>, (rc "Union" 1)))
                primitive (uc "C" 1 "Item" 0) Primitive.String
                primitive (uc "D" 2 "Item1" 0) Primitive.Int32
                primitive (uc "D" 2 "Item2" 1) Primitive.String
            |]
        let values = 
            [|
                primaryKey (rc "Id" 0)
                u unionValues
            |]
        let value = Record (typeof<UnionType>, values, Root)
        value

// ----------------------------------------------------------------------------

type [<Measure>] recordType

type E = {
    F: string
}

type RecordType = {
    Id: PrimaryKey<recordType>
    Record: E
}
with
    static member New =
        {
            Id = New
            Record = { F = "" }
        }
    static member ExpectedTable =
        let rc1 = recordContainer typeof<RecordType> Root
        let rc2 = recordContainer typeof<E> (rc1 "Record" 1)
        let r = record typeof<E> (rc1 "Record" 1)
        let recordValues =
            [|
                primitive (rc2 "F" 0) Primitive.String
            |]
        let values =
            [|
                primaryKey (rc1 "Id" 0)
                r recordValues
            |]
        let value = Record (typeof<RecordType>, values, Root)
        value

// ----------------------------------------------------------------------------

type [<Measure>] tupleType

type TupleType = {
    Id: PrimaryKey<tupleType>
    Tuple: string * int
}
with
    static member New =
        {
            Id = New
            Tuple = ("", 0)
        }
    static member ExpectedTable =
        let rc = recordContainer typeof<TupleType> Root
        let tc = tupleContainer
        let t = tuple typeof<Tuple<string, int>>
        let tupleValues =
            [|
                primitive (tc 0 (rc "Tuple" 1)) Primitive.String
                primitive (tc 1 (rc "Tuple" 1)) Primitive.Int32
            |]
        let values =
            [|
                primaryKey (rc "Id" 0)
                t (rc "Tuple" 1) tupleValues
            |]
        let value = Record (typeof<TupleType>, values, Root)
        value

// ----------------------------------------------------------------------------

(*
type [<Measure>] genericTypes

type GenericTypes = {
    Id: PrimaryKey<genericTypes>
    Option: string option
    Array: string []
    List: string list
    Seq: string seq
    Set: Set<string>
    Map: Map<string,string>
}
with
    static member New = 
        {
            Id = New
            Option = None
            Array = [||]
            List = []
            Seq = Seq.empty
            Set = Set.empty
            Map = Map.empty
        }
    static member ExpectedTable =
        let fields =
            [|
                f "Id"      0   typeof<PrimaryKey<genericTypes>>          (PrimaryKeyField)
                f "Option"  1   typeof<string option>       (OptionField ss)
                f "Array"   2   typeof<string []>           (ArrayField ss)
                f "List"    3   typeof<string list>         (ListField ss)
                f "Seq"     4   typeof<string seq>          (SeqField ss)
                f "Set"     5   typeof<Set<string>>         (SetField ss)
                f "Map"     6   typeof<Map<string,string>>  (MapField (ss, ss))
            |]
        RecordTable {
            Name = "GenericTypes"
            Type = typeof<GenericTypes>
            Fields = fields
        }
    member x.Key = 
        match x.Id with
        | New -> failwith "No key"
        | Existing id -> id

// ----------------------------------------------------------------------------

type [<Measure>] arrayTypes

type ArrayType = {
    Id: PrimaryKey<arrayTypes>
    Array: string []
}
with
    static member New = 
        {
            Id = New
            Array = [||]
        }
    static member ExpectedTable =
        let fields =
            [|
                f "Id"      0   typeof<PrimaryKey<arrayTypes>>  (PrimaryKeyField)
                f "Array"   1   typeof<string []>   (ArrayField ss)
            |]
        RecordTable {
            Name = "ArrayType"
            Type = typeof<ArrayType>
            Fields = fields
        }
    static member ExpectedForeignTable =
        let fields =
            [|
                f "Id"      0   typeof<PrimaryKey<DontCare>>          (PrimaryKeyField)
                f "Source"  1   typeof<ForeignKey<_>>  (ForeignKeyField)
                f "Index"   2   typeof<int>                 (PrimitiveField IntField)
                f "Value"   3   typeof<Value>               (ValueField)
            |]
        let recordTable = {
            Name = "ArrayType_Array"
            Type = typeof<ArrayElement>
            Fields = fields
        }
        ArrayTable {
            RecordTable = recordTable
            ValueType = s
        }
    member x.Key =
        match x.Id with
        | New -> failwith "No key"
        | Existing id -> id

// ----------------------------------------------------------------------------

(*
type [<Measure>] foreignKeyType

type ForeignKeyType = {
    Id: PrimaryKey<foreignKeyType>
    NullableType: ForeignKey<nullableType, NullableType>
}
with
    static member New = 
        {
            Id = New
            NullableType = ForeignKey 0<nullableType>
        }
    static member ExpectedTable =
        let fields = 
            [|
                f "Id"              0   typeof<PrimaryKey<foreignKeyType>>              (KeyField PrimaryKeyField)
                f "NullableType"    1   typeof<ForeignKey<nullableType, NullableType>>  (KeyField (ForeignKeyField NonNullable))
            |]
            |> Array.map (fun field -> field.Name, field)
            |> Map.ofArray
        {
            Name = "ForeignKeyType"
            Type = typeof<ForeignKeyType>
            Fields = fields
        }

// ----------------------------------------------------------------------------

type [<Measure>] nullableKeyType

type NullableKeyType = {
    Id: PrimaryKey<nullableKeyType>
    NullableKey: ForeignKey<nullableType, NullableType> option
}
with
    static member New = 
        {
            Id = New
            NullableKey = None
        }
    static member ExpectedTable =
        let fields = 
            [|
                f "Id"              0   typeof<PrimaryKey<foreignKeyType>>                      (KeyField PrimaryKeyField)
                f "NullableKey"     1   typeof<ForeignKey<nullableType, NullableType> option>   (KeyField (ForeignKeyField Nullable))
            |]
            |> Array.map (fun field -> field.Name, field)
            |> Map.ofArray
        {
            Name = "NullableKeyType"
            Type = typeof<NullableKeyType>
            Fields = fields
        }

// ----------------------------------------------------------------------------

type [<Measure>] tupleType

type TupleType = {
    Id: PrimaryKey<tupleType>
    Tuple: int * string
}
with
    static member New = 
        {
            Id = New
            Tuple = 0, ""
        }
    static member ExpectedTable =
        let fields = 
            [|
                f "Id"      0   typeof<PrimaryKey<foreignKeyType>>  (KeyField PrimaryKeyField)
                f "Tuple"   1   typeof<int * string>                (KeyField (ForeignKeyField Nullable))
            |]
            |> Array.map (fun field -> field.Name, field)
            |> Map.ofArray
        {
            Name = "NullableKeyType"
            Type = typeof<NullableKeyType>
            Fields = fields
        }
*)
type InnerRecord = {
    Numeric: int
    Text: string
}

type [<Measure>] innerRecordTest

type InnerRecordTest = {
    Id: PrimaryKey<innerRecordTest>
    Numeric: int
    Text: string
    InnerRecord: InnerRecord
}
with
    static member New = {
        Id = New
        Numeric = 10
        Text = "lol"
        InnerRecord = { Numeric = 11; Text = "lel" }
    }
    member x.Key = 
        match x.Id with
        | New -> failwith "No id"
        | Existing id -> id
    static member ExpectedTable =
        let innerFields = 
            [|
                { RecordField.Name = "Numeric"; Index = 0; ValueType = pf IntField }
                { Name = "Text"; Index = 1; ValueType = s }
            |]
        let fields = 
            [|
                f "Id"          0   typeof<PrimaryKey<innerRecordTest>>  (PrimaryKeyField)
                f "Numeric"     1   typeof<int>         (pf IntField)
                f "Text"        2   typeof<string>      (s)
                f "InnerRecord" 3   typeof<InnerRecord> (RecordField { Name = "InnerRecord"; Fields = innerFields })
            |]
        let recordTable = {
            Name = "InnerRecordTest"
            Type = typeof<InnerRecordTest>
            Fields = fields
        }
        RecordTable recordTable
    static member ForeignTable = 
        let fields =
            [|
                f "Numeric" 0 typeof<int>       (pf IntField)
                f "Text"    1 typeof<string>    (s)
            |]
        let recordTable = {
            Name = "InnerRecordTest_InnerRecord"
            Type = typeof<InnerRecord>
            Fields = fields
        }
        RecordTable recordTable
*)

type [<Measure>] accessTest
type [<Measure>] foreignAccessTest

type InnerRecord = {
    InnerRecordNumeric: int
    InnerRecordText: string
}
with
    static member New = {
        InnerRecordNumeric = 4
        InnerRecordText = "lel"
    }

type InnerUnion =
    | InnerUnionNumeric of int
    | InnerUnionText of string
with
    static member New = InnerUnionNumeric 5

type AccessTest = {
    PrimaryKey: PrimaryKey<accessTest>
    ForeignKey: ForeignKey<foreignAccessTest>
    Numeric: int
    Text: string
    InnerRecord: InnerRecord
    InnerUnion: InnerUnion
    Tuple: int * string
}
with
    static member New = {
        PrimaryKey = New
        ForeignKey = FsOrm.FsOrm.ForeignKey 2<foreignAccessTest>
        Numeric = 3
        Text = "lol"
        InnerRecord = InnerRecord.New
        InnerUnion = InnerUnion.New
        Tuple = 6, "kek"
    }

let insertId = ref 0
let getInsertId () =
    let i = !insertId
    insertId := i + 1
    i

type AccessTestTable () =
    interface IOrmTable<unused, obj> with
        member x.Insert t = 
            let key = getInsertId ()
            key * 1<unused>
        
        member x.Update _ = ()

        member x.Select _ = [||]
        member x.SelectKey k =
            match int k with
            | 0 -> InnerRecord.New :> obj
            | 1 -> InnerUnion.New :> obj

        member x.Delete _ = ()
        member x.DeleteKey _ = ()

type AccessTestDatabase () =
    interface IOrmInternalDatabase with
        member x.OpenTable value = 
            AccessTestTable () :> IOrmTable<_, _>
        member x.DeleteTable value = ()
            

// ----------------------------------------------------------------------------

open System.Data

[<TestClass>]
type ``Data tests`` () = 

    (*
    [<TestMethod>]
    member x.``Inner record test`` () =
        let value = InnerRecordTest.New
        let t = value.GetType ()
        let table = getTable None t
        let expectedTable = InnerRecordTest.ExpectedTable
        table =? expectedTable
        let foreignTables = expectedTable.ForeignTables ()
        foreignTables =? [| InnerRecordTest.ForeignTable |]
    *)

    [<TestMethod>]
    member x.``Access test`` () =
        let data = AccessTest.New
        let t = data.GetType ()
        let extract1 = extract t
        let create1 = create extract1
        let expectedCreate1 =
            [|
                { Name = "PrimaryKey"; Type = SqlType.Integer; KeyType = Some Primary }
                { Name = "ForeignKey"; Type = SqlType.Integer; KeyType = Some Foreign }
                { Name = "Numeric"; Type = SqlType.Integer; KeyType = None }
                { Name = "Text"; Type = SqlType.Text; KeyType = None }

                { Name = "InnerRecord_InnerRecordNumeric"; Type = SqlType.Integer; KeyType = None }
                { Name = "InnerRecord_InnerRecordText"; Type = SqlType.Text; KeyType = None }

                { Name = "InnerUnion_Tag"; Type = SqlType.Integer; KeyType = None }
                { Name = "InnerUnion_InnerUnionNumeric_0"; Type = SqlType.Integer; KeyType = None }
                { Name = "InnerUnion_InnerUnionText_0"; Type = SqlType.Text; KeyType = None }

                { Name = "Tuple_0"; Type = SqlType.Integer; KeyType = None }
                { Name = "Tuple_1"; Type = SqlType.Text; KeyType = None }
            |]
        create1 =? expectedCreate1

        let database = AccessTestDatabase () :> IOrmInternalDatabase

        // Insert test
        let insert = AccessTest.New :> obj
        let save1 = save Insert insert extract1
        let expectedSave1 = 
            [|
                //{ Name = "PrimaryKey"; DbType = DbType.Int32; Obj = 1 :> obj }
                { Name = "ForeignKey"; DbType = DbType.Int32; Obj =  2 :> obj }
                { Name = "Numeric"; DbType = DbType.Int32; Obj = 3 :> obj }
                { Name = "Text"; DbType = DbType.String; Obj = "lol" :> obj }

                { Name = "InnerRecord_InnerRecordNumeric"; DbType = DbType.Int32; Obj = 4 :> obj }
                { Name = "InnerRecord_InnerRecordText"; DbType = DbType.String; Obj = "lel" :> obj }

                { Name = "InnerUnion_Tag"; DbType = DbType.Int32; Obj = 0 :> obj }
                { Name = "InnerUnion_InnerUnionNumeric_0"; DbType = DbType.Int32; Obj = 5 :> obj }
                { Name = "InnerUnion_InnerUnionText_0"; DbType = DbType.String; Obj = null }

                { Name = "Tuple_0"; DbType = DbType.Int32; Obj = 6 :> obj }
                { Name = "Tuple_1"; DbType = DbType.String; Obj = "kek" :> obj }
            |]
        save1 =? expectedSave1

        // Update test
        let update = { AccessTest.New with PrimaryKey = Existing 1<accessTest> } :> obj
        let save1 = save Update update extract1
        let expectedSave1 = 
            [|
                //{ Name = "PrimaryKey"; DbType = DbType.Int32; Obj = 1 :> obj }
                { Name = "ForeignKey"; DbType = DbType.Int32; Obj =  2 :> obj }
                { Name = "Numeric"; DbType = DbType.Int32; Obj = 3 :> obj }
                { Name = "Text"; DbType = DbType.String; Obj = "lol" :> obj }

                { Name = "InnerRecord_InnerRecordNumeric"; DbType = DbType.Int32; Obj = 4 :> obj }
                { Name = "InnerRecord_InnerRecordText"; DbType = DbType.String; Obj = "lel" :> obj }

                { Name = "InnerUnion_Tag"; DbType = DbType.Int32; Obj = 0 :> obj }
                { Name = "InnerUnion_InnerUnionNumeric_0"; DbType = DbType.Int32; Obj = 5 :> obj }
                { Name = "InnerUnion_InnerUnionText_0"; DbType = DbType.String; Obj = null }

                { Name = "Tuple_0"; DbType = DbType.Int32; Obj = 6 :> obj }
                { Name = "Tuple_1"; DbType = DbType.String; Obj = "kek" :> obj }
            |]
        save1 =? expectedSave1

        // Load test
        let data1 = 
            [|
                "PrimaryKey", 1 :> obj
                "ForeignKey", 2 :> obj
                "Numeric", 3 :> obj
                "Text", "lol" :> obj

                "InnerRecord_InnerRecordNumeric", 4 :> obj
                "InnerRecord_InnerRecordText", "lel" :> obj

                "InnerUnion_Tag", 0 :> obj
                "InnerUnion_InnerUnionNumeric_0", 5 :> obj
                "InnerUnion_InnerUnionText_0", null :> obj

                "Tuple_0", 6 :> obj
                "Tuple_1", "kek" :> obj
            |]
            |> Map.ofArray
        let getter1 name = Map.find name data1
        let load1 = load getter1 extract1
        load1 =? update

    [<TestMethod>]
    member x.``Primary types`` () = 
        let value = PrimitiveTypes.New
        let t = value.GetType ()
        let table = extract t
        let expectedTable = PrimitiveTypes.ExpectedTable
        table =? expectedTable
        let name = tryGetTableName table
        Some "PrimitiveTypes" =? name

    [<TestMethod>]
    member x.``Union type`` () =
        let value = UnionType.NewB
        let t = value.GetType ()
        let table = extract t
        let expectedTable = UnionType.ExpectedTable
        table =? expectedTable
        let name = tryGetTableName table
        Some "UnionType" =? name

    [<TestMethod>]
    member x.``Record type`` () =
        let value = RecordType.New
        let t = value.GetType ()
        let table = extract t
        let expectedTable = RecordType.ExpectedTable
        table =? expectedTable
        let name = tryGetTableName table
        Some "RecordType" =? name

    [<TestMethod>]
    member x.``Tuple type`` () =
        let value = TupleType.New
        let t = value.GetType ()
        let table = extract t
        let expectedTable = TupleType.ExpectedTable
        table =? expectedTable
        let name = tryGetTableName table
        Some "TupleType" =? name

    (*
    [<TestMethod>]
    member x.``Generic types`` () =
        let value = GenericTypes.New
        let t = value.GetType ()
        let table = getTable None t
        let expectedTable = GenericTypes.ExpectedTable
        table =? expectedTable

    [<TestMethod>]
    member x.``Array type`` () =
        let value = ArrayType.New
        let t = value.GetType ()
        let table = getTable None t
        let expectedTable = ArrayType.ExpectedTable
        table =? expectedTable
        let foreignTables = table.ForeignTables ()
        foreignTables =? [| ArrayType.ExpectedForeignTable |]

    [<TestMethod>]
    member x.``Foreign key type`` () =
        let value = ForeignKeyType.New
        let t = value.GetType ()
        let table = getTable t
        match table with
        | failwith s ->
            raise <| AssertFailedException(s)
        |  table ->
            let expectedTable = ForeignKeyType.ExpectedTable
            table =? expectedTable
            let foreignKeyTables = table.getForeignKeyTables ()
            foreignKeyTables =?  [ NullableType.ExpectedTable ]

    [<TestMethod>]
    member x.``Nullable key type`` () =
        let value = NullableKeyType.New
        let t = value.GetType ()
        let table = getTable t
        match table with
        | failwith s ->
            raise <| AssertFailedException(s)
        |  table ->
            let expectedTable = NullableKeyType.ExpectedTable
            table =? expectedTable
            let foreignKeyTables = table.getForeignKeyTables ()
            foreignKeyTables =?  [ NullableType.ExpectedTable ]
    *)
        