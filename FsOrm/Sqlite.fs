﻿module FsOrm.Sqlite

open System
open System.ComponentModel
open System.Data
open System.IO
open System.Reflection

open Microsoft.FSharp.Reflection
open LanguagePrimitives

open System.Data.SQLite

open FsOrm.Data
open FsOrm.FsOrm
open FsOrm.Query
open FsOrm.Value
open FsOrm.Util

let reservedKeywords =
    [|
        "ABORT"; "ACTION"; "ADD"; "AFTER"; "ALL"; "ALTER"; "ANALYZE"; "AND"; "AS"; "ASC"; "ATTACH"
        "AUTOINCREMENT"; "BEFORE"; "BEGIN"; "BETWEEN"; "BY"; "CASCADE"; "CASE"; "CAST"; "CHECK"; "COLLATE"
        "COLUMN"; "COMMIT"; "CONFLICT"; "CONSTRAINT"; "CREATE"; "CROSS"; "CURRENT_DATE"; "CURRENT_TIME"; "CURRENT_TIMESTAMP"; "DATABASE"
        "DEFAULT"; "DEFERRABLE"; "DEFERRED"; "DELETE"; "DESC"; "DETACH"; "DISTINCT"; "DROP"; "EACH"; "ELSE"
        "END"; "ESCAPE"; "EXCEPT"; "EXCLUSIVE"; "EXISTS"; "EXPLAIN"; "FAIL"; "FOR"; "FOREIGN"; "FROM"
        "FULL"; "GLOB"; "GROUP"; "HAVING"; "IF"; "IGNORE"; "IMMEDIATE"; "IN"; "INDEX"; "INDEXED"
        "INITIALLY"; "INNER"; "INSERT"; "INSTEAD"; "INTERSECT"; "INTO"; "IS"; "ISNULL"; "JOIN"; "KEY"
        "LEFT"; "LIKE"; "LIMIT"; "MATCH"; "NATURAL"; "NO"; "NOT"; "NOTNULL"; "NULL"; "OF"
        "OFFSET"; "ON"; "OR"; "ORDER"; "OUTER"; "PLAN"; "PRAGMA"; "PRIMARY"; "QUERY"; "RAISE"
        "RECURSIVE"; "REFERENCES"; "REGEXP"; "REINDEX"; "RELEASE"; "RENAME"; "REPLACE"; "RESTRICT"; "RIGHT"; "ROLLBACK"
        "ROW"; "SAVEPOINT"; "SELECT"; "SET"; "TABLE"; "TEMP"; "TEMPORARY"; "THEN"; "TO"; "TRANSACTION"
        "TRIGGER"; "UNION"; "UNIQUE"; "UPDATE"; "USING"; "VACUUM"; "VALUES"; "VIEW"; "VIRTUAL"; "WHEN"
        "WHERE"; "WITH"; "WITHOUT"
    |]

type SqliteTable (value: Value, database: IOrmInternalDatabase, conn: SQLiteConnection) =
    let tableName = 
        tryGetTableName value
        |> Option.getOrFail "Cannot extract table name"

    let connectionCheck () =
        if conn.State <> ConnectionState.Open then 
            failwith "Connection is closed" 

    let tryGetId (obj: obj) =
        match value with
        | Record (_, values, _)
        | Union (_, values, _) ->
            values
            |> Array.tryPick (fun value ->
                match value with
                | PrimaryKey container ->
                    let obj = fetch obj container
                    match obj :?> PrimaryKey<_> with
                    | New -> None
                    | Existing id -> Some id
                | _ -> None
            )
        | _ -> None

    let tryGetIdName () =
        match value with
        | Record (_, values, _)
        | Union (_, values, _) ->
            values
            |> Array.tryPick (fun value ->
                match value with
                | PrimaryKey _ ->
                    Some <| getParameterName value
                | _ -> None
            )
        | _ -> None

    let createTable () =
        try
            let createParams = create value
            let content = 
                createParams
                |> Array.map (fun createParam -> createParam.ToSqlString)
                |> String.join ", "
            let sql = sprintf "CREATE TABLE %s (%s)" tableName content
            let command = new SQLiteCommand(sql, conn)
            TODO "Check the return of this"
            command.ExecuteNonQuery () |> ignore<int>
            ()
        with
        | e -> failwithf "Creating the table for type %s threw an exception: %s" tableName e.Message

    let insert (o_id: int option) (obj: obj) =
        let o_id = 
            o_id
            |> Option.bindIfNone (fun () -> tryGetId obj)
        let saveParameters = save Insert obj value
        let names =
            saveParameters
            |> Array.map (fun saveParameter ->
                saveParameter.Name
            )
            |> String.join ", "
        let parameters =
            saveParameters
            |> Array.map (fun saveParameter ->
                saveParameter.Param
            )
            |> String.join ", "
        let sql = sprintf "INSERT INTO %s (%s) VALUES (%s)" tableName names parameters
        let command = new SQLiteCommand(sql, conn)
        saveParameters
        |> Array.iter (fun saveParameter ->
            command.Parameters.Add(saveParameter.Param, saveParameter.DbType).Value <- saveParameter.Obj
        )
        TODO "Check count"
        let count = command.ExecuteNonQuery () 
        let sql = "SELECT last_insert_rowid()"
        let command = new SQLiteCommand(sql, conn)
        let lastId = command.ExecuteScalar ()
        let id = Convert.ToInt32(lastId)
        id

    let update (o_id: int option) (obj: obj) =
        let o_id =
            o_id
            |> Option.bindIfNone (fun () -> tryGetId obj)
        let saveParameters = save Update obj value
        let parameters =
            saveParameters
            |> Array.map (fun saveParameter ->
                sprintf "%s = %s" saveParameter.Name saveParameter.Param
            )
            |> String.join ", "
        let pkName =
            tryGetIdName ()
            |> Option.getOrFail "Can't find pk name"
        let pkParam = "@" + pkName
        let pk = 
            o_id 
            |> Option.getOrFail "No pk"
        let sql = sprintf "UPDATE %s SET %s WHERE %s = %s" tableName parameters pkName pkParam
        let command = new SQLiteCommand(sql, conn)
        saveParameters
        |> Array.iter (fun saveParameter ->
            command.Parameters.Add(saveParameter.Param, saveParameter.DbType).Value <- saveParameter.Obj
        )
        command.Parameters.Add(pkParam, DbType.Int32).Value <- pk
        TODO "Check count"
        let count = command.ExecuteNonQuery ()
        ()

    let selectKey (key: int) : obj =
        let pkName =
            tryGetIdName ()
            |> Option.getOrFail "Can't find pk name"
        let pkParam = "@" + pkName
        let names = 
            create value
            |> Array.map (fun createParameter ->
                createParameter.Name
            )
            |> String.join ", "
        let sql = sprintf "SELECT %s FROM %s WHERE %s = %s" names tableName pkName pkParam
        let command = new SQLiteCommand(sql, conn)
        command.Parameters.Add(pkParam, DbType.Int32).Value <- key
        let reader = command.ExecuteReader ()
        let hasRead = reader.Read ()
        let getter (name: string) = reader.[name]
        load getter value

    let select (query: Quotations.Expr<'t -> bool>) : obj [] =
        let where = parse query
        let names = 
            create value
            |> Array.map (fun createParameter ->
                createParameter.Name
            )
            |> String.join ", "
        let sql = sprintf "SELECT %s FROM %s WHERE %s" names tableName where
        let command = new SQLiteCommand(sql, conn)
        let reader = command.ExecuteReader ()
        let xs = 
            [|
                while reader.Read () do
                    let getter (name: string) = reader.[name]
                    let x = load getter value
                    yield x
                    
            |]
        xs
        
    let deleteKey (key: int) =
        let pkName =
            tryGetIdName ()
            |> Option.getOrFail "Can't find pk name"
        let pkParam = "@" + pkName
        let sql = sprintf "DELETE FROM %s WHERE %s = %s" tableName pkName pkParam
        let command = new SQLiteCommand(sql, conn)
        command.Parameters.Add(pkParam, DbType.Int32).Value <- key
        let count = command.ExecuteNonQuery ()
        ()

    let delete (obj: obj) =
        let id =
            tryGetId obj
            |> Option.getOrFail "Can't fetch pk"
        deleteKey id


    member x.TableName = tableName
    member x.CreateTable () = createTable ()
    member x.Insert (value: obj) = insert None value
    member x.InsertKey (key: int) (value: obj) = 
        let _ = insert (Some key) value
        ()
    member x.Update (value: obj) = update None value
    member x.UpdateKey (key: int) (value: obj) = update (Some key) value
    member x.Select (q: Quotations.Expr<'t -> bool>) = select q
    member x.SelectKey (key: int) = selectKey key
    member x.Delete (value: obj) = delete value
    member x.DeleteKey (key: int) = deleteKey key

// ----------------------------------------------------------------------------

type SqliteOrmTable<[<Measure>] 'k, 'v> (sqliteTable: SqliteTable) =

    interface IOrmTable<'k, 'v> with
        member x.Insert v = Int32WithMeasure<'k> <| sqliteTable.Insert v

        member x.Update v = sqliteTable.Update v

        member x.Select q = 
            sqliteTable.Select q
            |> Array.map (fun x -> x :?> 'v)
        member x.SelectKey k = sqliteTable.SelectKey (int k) :?> 'v

        member x.Delete v = sqliteTable.Delete v
        member x.DeleteKey k = sqliteTable.DeleteKey (int k)

// ----------------------------------------------------------------------------

type SqliteOrmDatabase (conn: SQLiteConnection) =
    let mutable tables : Map<string, SQLiteConnection * Value * SqliteTable> = Map.empty

    let tableExists (name: string) =
        let sql = sprintf "SELECT name FROM sqlite_master WHERE type='table' AND name='%s'" name
        let command = new SQLiteCommand(sql, conn)
        let result = command.ExecuteReader()
        result.HasRows

    member x.DeleteTable (value: Value) =
        match tryGetTableName value with
        | None -> ()
        | Some tableName ->
            let tableName = 
                tryGetTableName value
                |> Option.get
            let sql = sprintf "DROP TABLE IF EXISTS %s" tableName
            let command = new SQLiteCommand(sql, conn)
            command.ExecuteNonQuery () |> ignore<int>

            tables <- Map.remove tableName tables

    member x.OpenTable (value: Value) =
        match tryGetTableName value with
        | None -> raise InvalidTableException
        | Some tableName ->
            tables
            |> Map.tryFind tableName
            |> (function
                | None ->
                    let ormTable = SqliteTable(value, x :> IOrmInternalDatabase, conn)
                    if not <| tableExists ormTable.TableName then
                        ormTable.CreateTable ()
                    
                    tables <- Map.add tableName (conn, value, ormTable) tables
                    ormTable
                | Some (_, _, table) -> table
            )

    interface IOrmInternalDatabase with
        member x.OpenTable value = 
            let table = x.OpenTable value
            let ormTable = SqliteOrmTable<unused, obj> table
            ormTable :> IOrmTable<unused, obj>
        member x.DeleteTable value = x.DeleteTable value

    interface IOrmDatabase with
        member x.OpenTable<[<Measure>] 'k, 't> () : IOrmTable<'k, 't> =
            if conn.State <> ConnectionState.Open then failwith "Connection is closed" else
            let t = typeof<'t>
            let name = t.Name
            tables
            |> Map.tryFind name
            |> (function
                | Some (_, _, table) ->
                    SqliteOrmTable<'k, 't>(table) :> IOrmTable<'k, 't>
                | None ->
                    let table = extract t
                    let ormTable = x.OpenTable table
                    SqliteOrmTable<'k, 't>(ormTable) :> IOrmTable<'k, 't>
            )
        
        member x.CloseTable<'t> () =
            failwith "Not implemented"

        member x.DeleteTable<'t> () =
            let t = typeof<'t>
            let value = extract t
            x.DeleteTable value

// ----------------------------------------------------------------------------

type SqliteOrm () =
    let mutable databases : Map<string, SQLiteConnection * SqliteOrmDatabase> = Map.empty
    interface IOrm with
        member x.OpenDatabase connectionString =
            let database = 
                match Map.tryFind connectionString databases with
                | Some (_, database) -> 
                    database
                | None ->
                    let conn = new SQLiteConnection(sprintf "Data Source=%s;Version=3;" connectionString)
                    conn.Open ()
                    let database = new SqliteOrmDatabase(conn)
                    databases <- Map.add connectionString (conn, database) databases
                    database
            database :> IOrmDatabase

        member x.CloseDatabase connectionString =
            match Map.tryFind connectionString databases with
            | None -> ()
            | Some (conn, _) ->
                conn.Close ()
                conn.Dispose ()
                databases <- Map.remove connectionString databases
                ()

        member x.DeleteDatabase connectionString =
            try
                match Map.tryFind connectionString databases with
                | None -> 
                    ()
                | Some (conn, _) ->
                    conn.Close ()
                    conn.Dispose ()
                File.Delete connectionString
                databases <- Map.remove connectionString databases
                ()
            with
            | e -> failwithf "Deleting the database `%s` threw an exception: %s" connectionString e.Message
