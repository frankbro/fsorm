﻿module FsOrm.Util

open System

[<Obsolete>]
let inline TODO _ = ()

module Option =
    let defaultsTo (x: 't) (o: 't option) : 't =
        match o with
        | Some x    -> x
        | None      -> x

    let bindIfNone (f: unit -> 't option) (x: 't option) : 't option =
        match x with
        | Some x -> Some x
        | None -> f ()

    let getOrFail (msg: string) (x: 't option) : 't =
        match x with
        | Some x -> x
        | None -> failwith msg

module Map =
    let keys map =
        map
        |> Map.toArray
        |> Array.map fst 

    let values map =
        map
        |> Map.toArray
        |> Array.map snd

module String =
    let join (sep: string) (xs: string []) = String.Join(sep, xs)