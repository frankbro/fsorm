﻿module FsOrm.Value

open System

open FsOrm.FsOrm
open FsOrm.Util

type Primitive = 
    | Boolean
    | Byte
    | SByte
    | Int16
    | UInt16
    | Int32
    | UInt32
    | Int64
    | UInt64
    | Char
    | String
    | Float32
    | Float
    | DateTime
    | Guid

TODO "Keep the name of uniontag and internalprimarykey if clash"

type RecordContainer = {
    Container: Container
    RecordType: Type
    FieldName: string
    FieldIndex: int
}

and UnionContainer = {
    Container: Container
    UnionType: Type
    UnionTagName: string
    UnionTagIndex: int
    ArgName: string
    ArgIndex: int
}

and TupleContainer = {
    Container: Container
    Index: int
}

and Container =
    | Root
    | RecordContainer of RecordContainer
    | UnionContainer of UnionContainer
    | TupleContainer of TupleContainer
    | UnionTag of Type * Container

and Value =
    | PrimaryKey of Container
    | ForeignKey of Container
    | Tag of Container
    | Primitive of Primitive * Container 
    | Record of Type * Value [] * Container 
    | Union of Type * Value [] * Container 
    | Tuple of Type * Value [] * Container 
with
    member x.Container = 
        match x with
        | PrimaryKey container
        | ForeignKey container
        | Tag container
        | Primitive (_, container)
        | Record (_, _, container)
        | Union (_, _, container)
        | Tuple (_, _, container) ->
            container

type [<Measure>] unused

type IOrmInternalDatabase =
    abstract member OpenTable : Value -> IOrmTable<unused, obj>
    abstract member DeleteTable : Value -> unit
