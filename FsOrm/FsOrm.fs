﻿module FsOrm.FsOrm

open System

type ICustomSerializer = 
    abstract Serialize: obj -> string
    abstract Deserialize: string -> obj

type CustomSerialization (sty: Type) =
    inherit Attribute ()

    member x.CustomSerializer = Activator.CreateInstance(sty) :?> ICustomSerializer

exception PrimaryKeyGetNewException

// ----------------------------------------------------------------------------

type PrimaryKey<[<Measure>] 't> =
    | New
    | Existing of int<'t>
with
    member x.Get = 
        match x with
        | New -> raise PrimaryKeyGetNewException
        | Existing id -> id

type ForeignKey<[<Measure>] 't> =
    | ForeignKey of int<'t>

// ----------------------------------------------------------------------------

type IOrmTable<[<Measure>] 'k, 't> =
    abstract Insert : 't -> int<'k>

    abstract Update : 't -> unit
    //abstract UpdateQuery : Quotations.Expr<'t -> bool> -> ('t -> 't) -> 't []

    abstract Select : Quotations.Expr<'t -> bool> -> 't []
    abstract SelectKey : int<'k> -> 't

    abstract Delete : 't -> unit
    abstract DeleteKey : int<'k> -> unit
    //abstract DeleteQuery : Quotations.Expr<bool> -> unit

type IOrmDatabase =
    /// Return a table on which you can save/load instances. Create the table if it doesn't exist. 
    /// Might need to create extra tables for foreign key capabilities. Verify type matches table on open.
    abstract OpenTable<[<Measure>] 'k, 'v> : unit -> IOrmTable<'k, 'v>
    /// Cleanup of any of the state needed.
    abstract CloseTable<'t> : unit -> unit
    /// Delete table. Returns success even if the table didn't exist.
    abstract DeleteTable<'t> : unit -> unit

type IOrm =
    /// Open a connection to a database. Create the database if it doesn't exist (if possible)
    abstract OpenDatabase : connectionString:string -> IOrmDatabase
    /// Close a connection to a database.
    abstract CloseDatabase : connectionString:string -> unit
    /// Delete a database
    abstract DeleteDatabase : connectionString:string -> unit

// ----------------------------------------------------------------------------
