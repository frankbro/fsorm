﻿module FsOrm.Data

open System
open System.Data
open System.Reflection

open Microsoft.FSharp.Reflection

open FsOrm.FsOrm
open FsOrm.Value
open FsOrm.Util

// ----------------------------------------------------------------------------

let tryGetTableName (value: Value) =
    match value with
    | PrimaryKey _
    | ForeignKey _
    | Tag _ 
    | Primitive _ -> None
    | Record (t, _, _) -> Some t.Name
    | Union (t, _, _) -> Some t.Name
    | Tuple _ -> None

// ----------------------------------------------------------------------------

type SqlType =
    | Integer
    | Real
    | Text
with
    override x.ToString () =
        match x with
        | Integer -> "INTEGER"
        | Real -> "REAL"
        | Text -> "TEXT"

type KeyType =
    | Primary
    | Foreign

type CreateParameter = {
    Name: string
    Type: SqlType
    KeyType: KeyType option
}
with
    member x.ToSqlString = 
        let suffix = 
            if x.KeyType = Some Primary then
                " PRIMARY KEY AUTOINCREMENT"
            else
                ""
        sprintf "%s %O%s" x.Name x.Type suffix

let [<Literal>] InternalPrimaryKeyName = "Id"
let [<Literal>] UnionTagName = "Tag"

exception InvalidTableException

let getParameterName (value: Value) =
    let rec loop name container =
        match container with
        | Root -> name
        | RecordContainer { Container = container; FieldName = fieldName } ->
            let name = 
                if name = "" then
                    fieldName
                else
                    fieldName + "_" + name
            loop name container
        | UnionContainer { Container = container; UnionTagName = unionTagName; ArgIndex = argIndex } ->
            let name = 
                if name = "" then
                    unionTagName + "_" + string argIndex
                else
                    unionTagName + "_" + string argIndex + "_" + name
            loop name container
        | TupleContainer { Index = index; Container = container } ->
            let name = loop name container
            if name = "" then
                raise InvalidTableException
            else
                name + "_" + string index
        | UnionTag (_, container) ->
            let name = 
                if name = "" then
                    UnionTagName
                else 
                    UnionTagName + "_" + name
            loop name container
    loop "" value.Container

let primitiveCreateParameter (name: string) (primitive: Primitive) =
    let sqlType =
        match primitive with
        | Boolean   -> SqlType.Integer
        | Byte      -> SqlType.Integer
        | SByte     -> SqlType.Integer
        | Int16     -> SqlType.Integer
        | UInt16    -> SqlType.Integer
        | Int32     -> SqlType.Integer
        | UInt32    -> SqlType.Integer
        | Int64     -> SqlType.Integer
        | UInt64    -> SqlType.Integer
        | Char      -> SqlType.Text
        | String    -> SqlType.Text
        | Float32   -> SqlType.Real
        | Float     -> SqlType.Real
        | DateTime  -> SqlType.Integer
        | Guid      -> SqlType.Text
    { Name = name; Type = sqlType; KeyType = None }

let create (value: Value) =
    let rec loop isRoot value =
        match value with
        | PrimaryKey _ ->
            if isRoot then
                raise InvalidTableException
            else
                [| { Name = getParameterName value; Type = SqlType.Integer; KeyType = Some Primary } |]
        | ForeignKey _ ->
            if isRoot then
                raise InvalidTableException
            else
                [| { Name = getParameterName value; Type = SqlType.Integer; KeyType = Some Foreign } |]
        | Tag _ ->
            if isRoot then
                raise InvalidTableException
            else
                [| { Name = getParameterName value; Type = SqlType.Integer; KeyType = None } |]
        | Primitive (primitive, _) ->
            if isRoot then
                raise InvalidTableException
            else
                [| primitiveCreateParameter (getParameterName value) primitive |]
        | Record (_, values, _) 
        | Union (_, values, _) ->
            values
            |> Array.collect (fun value ->
                loop false value
            )
        | Tuple (_, values, _) ->
            if isRoot then
                raise InvalidTableException
            else
                values
                |> Array.collect (fun value ->
                    loop isRoot value
                )
    loop true value

// ----------------------------------------------------------------------------

type SaveParameter = {
    Name: string
    DbType: DbType
    Obj: obj
}
with
    member x.Param = "@" + x.Name

let fetch (obj: obj) (container: Container) =
    match container with
    | Root ->
        obj
    | RecordContainer { RecordType = recordType; FieldName = fieldName } ->
        recordType.GetProperty(fieldName).GetValue(obj)
    | UnionContainer { UnionType = unionType; UnionTagIndex = unionTagIndex; ArgIndex = argIndex } ->
        let (info, fields) = FSharpValue.GetUnionFields (obj, unionType)
        if info.Tag <> unionTagIndex then null else
        assert(argIndex <= Array.length fields)
        fields.[argIndex]
    | TupleContainer { Index = index } ->
        FSharpValue.GetTupleField (obj, index)
    | UnionTag (unionType, _) ->
        let (info, _) = FSharpValue.GetUnionFields (obj, unionType)
        info.Tag :> obj

let primitiveSaveParameters (name: string) (obj: obj) (primitive: Primitive) =
    let dbType =
        match primitive with
        | Boolean   -> DbType.Boolean
        | Byte      -> DbType.Byte
        | SByte     -> DbType.SByte
        | Int16     -> DbType.Int16
        | UInt16    -> DbType.UInt16
        | Int32     -> DbType.Int32
        | UInt32    -> DbType.UInt32
        | Int64     -> DbType.Int64
        | UInt64    -> DbType.UInt64
        | Char      -> DbType.String
        | String    -> DbType.String
        | Float32   -> DbType.Single
        | Float     -> DbType.Double
        | DateTime  -> DbType.Int64
        | Guid      -> DbType.String
    let obj =
        match primitive with
        | DateTime -> 
            let dateTime = obj :?> DateTime
            let ticks = dateTime.Ticks
            ticks :> obj
        | Guid ->
            let guid = obj :?> Guid
            let str = string guid
            str :> obj
        | _ ->
            obj
    [| { Name = name; DbType = dbType; Obj = obj } |]

type SaveMethod =
    | Insert
    | Update

let save (saveMethod: SaveMethod) (obj: obj) (value: Value) =
    let rec loop (isRoot: bool) (obj: obj) (value: Value) =
        match value with
        | PrimaryKey container ->
            match saveMethod with
            | Insert ->
                let obj = fetch obj value.Container
                match obj :?> PrimaryKey<_> with
                | New -> [||]
                | Existing id ->
                    let name = getParameterName value
                    [| { Name = name; DbType = DbType.Int32; Obj = id :> obj } |]
            | Update -> [||]
        | ForeignKey container ->
            let obj = fetch obj value.Container
            match obj :?> ForeignKey<_> with
            | ForeignKey.ForeignKey id ->
                let name = getParameterName value
                [| { Name = name; DbType = DbType.Int32; Obj = id :> obj } |]
        | Tag _ ->
            let obj = fetch obj value.Container
            let name = getParameterName value
            [| { Name = name; DbType = DbType.Int32; Obj = obj } |]
        | Primitive (primitive, container) ->
            let name = getParameterName value
            let obj = fetch obj value.Container
            primitiveSaveParameters name obj primitive
        | Record (t, recordValues, container) ->
            let obj =
                if isRoot then
                    obj
                else
                    fetch obj value.Container
            recordValues
            |> Array.collect (fun recordValue ->
                loop false obj recordValue
            )
        | Union (t, unionValues, container) ->
            let obj =
                if isRoot then
                    obj
                else
                    fetch obj value.Container
            unionValues
            |> Array.collect (fun unionValue ->
                loop false obj unionValue
            )
        | Tuple (t, tupleValues, container) ->
            let obj = fetch obj value.Container
            tupleValues
            |> Array.collect (fun tupleValue ->
                loop isRoot obj tupleValue
            )
    loop true obj value

// ----------------------------------------------------------------------------

let primitiveLoad (obj: obj) primitive : obj =
    match primitive with
    | Boolean -> Convert.ToBoolean obj :> obj
    | Byte -> Convert.ToByte obj :> obj
    | SByte -> Convert.ToSByte obj :> obj
    | Int16 -> Convert.ToInt16 obj :> obj
    | UInt16 -> Convert.ToUInt16 obj :> obj
    | Int32 -> Convert.ToInt32 obj :> obj
    | UInt32 -> Convert.ToUInt32 obj :> obj
    | Int64 -> Convert.ToInt64 obj :> obj
    | UInt64 -> Convert.ToUInt64 obj :> obj
    | Char -> Convert.ToChar obj :> obj
    | String -> Convert.ToString obj :> obj
    | Float32 -> Convert.ToSingle obj :> obj
    | Float -> Convert.ToDouble obj :> obj
    | DateTime ->
        let ticks = Convert.ToInt64 obj
        let dateTime = new DateTime(ticks)
        dateTime :> obj
    | Guid -> 
        let str = Convert.ToString obj
        let guid = new Guid(str)
        guid :> obj

let load (getter: string -> obj) (value: Value) =
    let rec loop (isRoot: bool) (value: Value) =
        match value with
        | PrimaryKey container ->
            let name = getParameterName value
            let obj = getter name
            let id = Convert.ToInt32 obj
            Existing id :> obj
            |> Some
        | ForeignKey container ->
            let name = getParameterName value
            let obj = getter name
            let id = Convert.ToInt32 obj
            ForeignKey.ForeignKey id :> obj
            |> Some
        | Tag _ -> 
            None
        | Primitive (primitive, container) ->
            let name = getParameterName value
            let obj = getter name
            primitiveLoad obj primitive
            |> Some
        | Record (t, recordValues, container) ->
            let values =
                recordValues
                |> Array.choose (fun recordValue ->
                    loop false recordValue
                )
            FSharpValue.MakeRecord (t, values)
            |> Some
        | Union (t, unionValues, container) ->
            let tagValue = Tag (UnionTag (t, container))
            let tagName = getParameterName tagValue
            let tagObj = getter tagName
            let tag = Convert.ToInt32(tagObj)
            let values =
                unionValues
                |> Array.choose (fun unionValue ->
                    match unionValue.Container with
                    | UnionContainer { UnionTagIndex = t } when t = tag ->
                        loop false unionValue
                    | _ ->
                        None
                )
            let caseInfos = FSharpType.GetUnionCases t
            let caseInfo =
                caseInfos
                |> Array.find (fun caseInfo -> caseInfo.Tag = tag)
            FSharpValue.MakeUnion (caseInfo, values)
            |> Some
        | Tuple (t, tupleValues, container) ->
            let values =
                tupleValues
                |> Array.choose (fun tupleValue ->
                    loop isRoot tupleValue
                )
            FSharpValue.MakeTuple (values, t)
            |> Some
    match loop true value with
    | None -> raise InvalidTableException
    | Some x -> x

// ----------------------------------------------------------------------------

let tryPrimaryKeyExtract (container: Container) (ta: Type) : Value option =
    if ta = typeof<PrimaryKey<_>> then
        PrimaryKey container
        |> Some
    else
        None

let tryForeignKeyExtract (container: Container) (ta: Type) : Value option =
    if ta = typeof<ForeignKey<_>> then
        ForeignKey container
        |> Some
    else
        None

let supportedPrimitiveTypes = 
    [|
        typeof<bool>,       Primitive.Boolean
        typeof<byte>,       Primitive.Byte
        typeof<sbyte>,      Primitive.SByte
        typeof<int16>,      Primitive.Int16
        typeof<uint16>,     Primitive.UInt16
        typeof<int>,        Primitive.Int32
        typeof<uint32>,     Primitive.UInt32
        typeof<int64>,      Primitive.Int64
        typeof<uint64>,     Primitive.UInt64
        typeof<char>,       Primitive.Char
        typeof<string>,     Primitive.String
        typeof<float32>,    Primitive.Float32
        typeof<float>,      Primitive.Float
        typeof<DateTime>,   Primitive.DateTime
        typeof<Guid>,       Primitive.Guid
    |]

let tryPrimitiveExtract (container: Container) (ta: Type) : Value option =
    supportedPrimitiveTypes
    |> Array.tryFind (fun (tb, _) -> ta = tb)
    |> Option.map (fun (_, primitive) ->
        Primitive (primitive, container)
    )

let isPrimaryKey (value: Value) =
    match value with
    | PrimaryKey _ -> true
    | _ -> false

let rec tryRecordExtract (container: Container) (t: Type) =
    if not <| FSharpType.IsRecord t then None else
    let recordFields = FSharpType.GetRecordFields t
    let values : Value [] =
        recordFields
        |> Array.mapi (fun index property -> 
            let container = 
                RecordContainer {
                    Container = container
                    RecordType = t
                    FieldName = property.Name
                    FieldIndex = index
                }
            let value = 
                tryExtract container property.PropertyType
                |> Option.get
            value
        )
    (t, values, container)
    |> Record
    |> Some

and tryUnionExtract (container: Container) (t: Type) =
    if not <| FSharpType.IsUnion t then None else
    let unionCases = FSharpType.GetUnionCases t
    let values : Value [] = 
        unionCases
        |> Array.collect (fun case ->
            let fields = case.GetFields ()
            let values = 
                fields
                |> Array.mapi (fun index field ->
                    let container = 
                        UnionContainer {
                            Container = container
                            UnionType = t
                            UnionTagName = case.Name
                            UnionTagIndex = case.Tag
                            ArgName = field.Name
                            ArgIndex = index
                        }
                    let value = 
                        tryExtract container field.PropertyType
                        |> Option.get
                    value
                )
            values
        )
        |> Array.append [| Tag (UnionTag (t, container)) |]
    (t, values, container)
    |> Union
    |> Some

and tryTupleExtract (container: Container) (t: Type) =
    if not <| FSharpType.IsTuple t then None else
    let tupleElements = FSharpType.GetTupleElements t
    let values =
        tupleElements
        |> Array.mapi (fun index tupleElement ->
            let container = 
                TupleContainer {
                    Container = container
                    Index = index
                }
            let value = 
                tryExtract container tupleElement
                |> Option.get
            value
        )
    (t, values, container)
    |> Tuple
    |> Some

and tryExtract (container: Container) (t: Type) = 
    None
    |> Option.bindIfNone (fun () -> tryPrimaryKeyExtract container t)
    |> Option.bindIfNone (fun () -> tryForeignKeyExtract container t)
    |> Option.bindIfNone (fun () -> tryPrimitiveExtract container t)
    |> Option.bindIfNone (fun () -> tryRecordExtract container t)
    |> Option.bindIfNone (fun () -> tryUnionExtract container t)
    |> Option.bindIfNone (fun () -> tryTupleExtract container t)

let extract (t: Type) =
    tryExtract Root t
    |> Option.get

// ----------------------------------------------------------------------------

let tryGetAttribute<'t when 't :> Attribute> (property: PropertyInfo) : 't option =
    let attrs = 
        property.GetCustomAttributes<'t>()
        |> Array.ofSeq
    match attrs with
    | [| attr |] -> Some attr
    | _ -> None

(*
and tryGetOptionField (t: Type) =
    if t.IsGenericType && t.GetGenericTypeDefinition () = typedefof<option<_>> then
        match t.GetGenericArguments () with
        | [| innerType |] ->
            let o_ValueType = tryGetValueType innerType
            o_ValueType
            |> Option.map (fun valueType ->
                OptionField { Type = innerType; ValueType = valueType }
            )
        | _ -> None
    else
        None

and tryGetArrayField (t: Type) =
    if t.IsArray then
        let innerType =  t.GetElementType () 
        let o_ValueType = tryGetValueType innerType
        o_ValueType
        |> Option.map (fun valueType ->
            ArrayField { Type = innerType; ValueType = valueType }
        )
    else
        None

and tryGetListField (t: Type) =
    if t.IsGenericType && t.GetGenericTypeDefinition() = typedefof<List<_>> then
        match t.GetGenericArguments () with
        | [| innerType |] ->
            let o_ValueType = tryGetValueType innerType
            o_ValueType
            |> Option.map (fun valueType ->
                ListField { Type = innerType; ValueType = valueType }
            )
        | _ -> None
    else
        None

and tryGetSeqField (t: Type) =
    if t.IsGenericType && t.GetGenericTypeDefinition () = typedefof<seq<_>> then
        match t.GetGenericArguments () with
        | [| innerType |] ->
            let o_ValueType = tryGetValueType innerType
            o_ValueType
            |> Option.map (fun valueType ->
                SeqField { Type = innerType; ValueType = valueType }
            )
        | _ -> None
    else
        None

and tryGetSetField (t: Type) =
    if t.IsGenericType && t.GetGenericTypeDefinition () = typedefof<Set<_>> then
        match t.GetGenericArguments () with
        | [| innerType |] ->
            let o_ValueType = tryGetValueType innerType
            o_ValueType
            |> Option.map (fun valueType ->
                SetField { Type = innerType; ValueType = valueType }
            )
        | _ -> None
    else
        None

and tryGetMapField (t: Type) =
    if t.IsGenericType && t.GetGenericTypeDefinition () = typedefof<Map<_,_>> then
        match t.GetGenericArguments () with
        | [| ta; tb |] ->
            let o_taField = tryGetValueType ta
            let o_tbField = tryGetValueType tb
            match o_taField, o_tbField with
            | Some taField, Some tbField ->
                Some <| MapField ( { Type = ta; ValueType = taField } , { Type = tb; ValueType = tbField } )
            | _, _ -> None
        | _ -> None
    else
        None
*)