﻿module FsOrm.Query

open System

open Microsoft.FSharp.Quotations
open Microsoft.FSharp.Reflection
module P = Microsoft.FSharp.Quotations.Patterns
module DP = Microsoft.FSharp.Quotations.DerivedPatterns

open FsOrm.FsOrm
open FsOrm.Util

exception InvalidQueryException of string

let between (x: 'a) (a: 'a) (b: 'a) = true
let like (x: string) (s: string) = true
let isIn (x: 'a) (xs: 'a []) = true

module private Private =
    type SqlValue =
        | Numeric of string
        | Text of string
    with
        static member FromValue (value: 't) =
            match box value with
            | :? PrimaryKey<_> as x ->
                match x with
                | New -> raise <| InvalidQueryException "Cannot query on a new primary key."
                | Existing id -> Numeric <| sprintf "%d" id
            | :? ForeignKey<_> as x ->
                match x with
                | ForeignKey id -> Numeric <| sprintf "%d" id
            | :? bool as x          -> Numeric (if x then "1" else "0")
            | :? byte as x          -> Numeric <| sprintf "%d" x
            | :? sbyte as x         -> Numeric <| sprintf "%d" x
            | :? int16 as x         -> Numeric <| sprintf "%d" x
            | :? uint16 as x        -> Numeric <| sprintf "%d" x
            | :? int32 as x         -> Numeric <| sprintf "%d" x
            | :? uint32 as x        -> Numeric <| sprintf "%d" x
            | :? int64 as x         -> Numeric <| sprintf "%d" x
            | :? uint64 as x        -> Numeric <| sprintf "%d" x
            | :? char as x          -> Text <| sprintf "'%c'" x
            | :? string as x        -> Text <| sprintf "'%s'" x
            //| :? decimal as x       -> Numeric <| sprintf "%f" x
            | :? float32 as x       -> Numeric <| sprintf "%f" x
            | :? float as x         -> Numeric <| sprintf "%f" x
            | :? DateTime as x      -> Numeric <| sprintf "%d" x.Ticks
            | :? Guid as x          -> Text <| sprintf "'%s'" (string x)
            | _ -> 
                raise <| InvalidQueryException (sprintf "Unknown type of value: %s" typeof<'t>.Name)
        override x.ToString () =
            match x with
            | Numeric s
            | Text s -> s

    type SqlUnion = {
        TagName: string
        TagIndex: int
        Values: Value []
    }
    with
        override x.ToString () =
            sprintf "%s (%d), %A" x.TagName x.TagIndex x.Values

    and Value =
        | SqlValue of SqlValue
        | SqlValues of SqlValue []
        | SqlRecord of Map<string, Value>
        | SqlUnion of SqlUnion
        | SqlTuple of Value []
    with
        override x.ToString () =
            match x with
            | SqlValue sqlValue -> string sqlValue
            | SqlValues sqlValues -> sprintf "%A" sqlValues
            | SqlRecord args -> sprintf "%A" args
            | SqlUnion union -> sprintf "%O" union
            | SqlTuple args -> sprintf "(%A)" args

    type QueryExpr =
        | Value of Value
        | FieldName of string
        | Operator of Operator
        | And of QueryExpr * QueryExpr
        | Or of QueryExpr * QueryExpr
        | Not of QueryExpr
        | Between of QueryExpr * QueryExpr * QueryExpr
        | Like of QueryExpr * QueryExpr
        | In of QueryExpr * QueryExpr

    and Operator =
        | Equal of QueryExpr * QueryExpr
        | NotEqual of QueryExpr * QueryExpr
        | GreaterThan of QueryExpr * QueryExpr
        | LesserThan of QueryExpr * QueryExpr
        | GreaterOrEqualTo of QueryExpr * QueryExpr
        | LesserOrEqualTo of QueryExpr * QueryExpr
    with 
        member x.GetSides = 
            match x with
            | Equal (a, b)
            | NotEqual (a, b)
            | GreaterThan (a, b)
            | LesserThan (a, b)
            | GreaterOrEqualTo (a, b)
            | LesserOrEqualTo (a, b) -> a, b

        member x.GetOperatorString =
            match x with
            | Equal (a, b) -> " = "
            | NotEqual (a, b) -> " <> "
            | GreaterThan (a, b) -> " > "
            | LesserThan (a, b) -> " < "
            | GreaterOrEqualTo (a, b) -> " >= "
            | LesserOrEqualTo (a, b) -> " <= "

    let rec operatorString (o: Operator) : string =
        let sides = o.GetSides
        let operatorString = o.GetOperatorString
        match sides with
        | FieldName name, Value(SqlRecord map)
        | Value(SqlRecord map), FieldName name ->
            map
            |> Map.toArray
            |> Array.map (fun (fieldName, fieldValue) ->
                name + "_" + fieldName + operatorString + string fieldValue
            )
            |> String.concat " && "
        | FieldName name, Value(SqlTuple args)
        | Value(SqlTuple args), FieldName name ->
            args
            |> Array.mapi (fun i arg ->
                name + "_" + string i + operatorString + string arg
            )
            |> String.concat " && "
        | FieldName name, Value(SqlUnion union)
        | Value(SqlUnion union), FieldName name ->
            let stringArgs =
                union.Values
                |> Array.mapi (fun i value ->
                    name + "_" + union.TagName + "_" + string i + operatorString + string value
                )
                |> String.concat " && "
            TODO "Dont hardcore tag"
            name + "_Tag = " + string union.TagIndex + " && " + stringArgs
        | _, _ ->
            let a, b = sides
            queryExprString a + operatorString + queryExprString b

    and queryExprString (q: QueryExpr) : string =
        match q with
        | Value(SqlValue value) -> string value
        | Value(SqlValues values) ->
            values
            |> Array.map string
            |> String.join ", "
        | Value(SqlRecord _) -> raise <| InvalidQueryException "Orphan SqlRecord"
        | Value(SqlUnion _) -> raise <| InvalidQueryException "Orphan SqlUnion"
        | FieldName name ->
            name
        | Operator o -> operatorString o
        | And (a, b) -> queryExprString a + " AND " + queryExprString b
        | Or (a, b) -> queryExprString a + " OR " + queryExprString b
        | Not x -> "NOT " + queryExprString x
        | Between (x, a, b) ->
            queryExprString x + " BETWEEN " + queryExprString a + " AND " + queryExprString b
        | Like (x, s) ->
            queryExprString x + " LIKE " + queryExprString s
        | In (x, xs) ->
            queryExprString x + " IN (" + queryExprString xs + ")"

    let rec eval expr =
        match expr with
        | P.Value (value, _) -> 
            value
        | P.PropertyGet (Some instance, pi, args) ->
            pi.GetValue(eval instance, evalAll args)
        | P.PropertyGet (None, pi, args) ->
            pi.GetValue(null, evalAll args)
        | P.Call (Some instance, mi, args) ->
            mi.Invoke(eval instance, evalAll args)
        | P.Call (None, mi, args) ->
            mi.Invoke(null, evalAll args)
        | _ -> raise <| InvalidQueryException (sprintf "Invalid value expression: %O" expr)
    
    and evalAll exprs =
        exprs |> List.map eval |> List.toArray

    type State = {
        Subject: string option
        Env: Map<string, Value>
    }
    with
        static member New = {
            Subject = None
            Env = Map.empty
        }

    let rec parse (state: State) expr =
        match expr with
        // --------------------------------------------------------------------
        | DP.SpecificCall <@@ (=) @@> (_, _, [a; b]) ->
            Operator (Equal (parse state a, parse state b))
        | DP.SpecificCall <@@ (<>) @@> (_, _, [a; b]) ->
            Operator (NotEqual (parse state a, parse state b))
        | DP.SpecificCall <@@ (>) @@> (_, _, [a; b]) ->
            Operator (GreaterThan (parse state a, parse state b))
        | DP.SpecificCall <@@ (<) @@> (_, _, [a; b]) ->
            Operator (LesserThan (parse state a, parse state b))
        | DP.SpecificCall <@@ (>=) @@> (_, _, [a; b]) ->
            Operator (GreaterOrEqualTo (parse state a, parse state b))
        | DP.SpecificCall <@@ (<=) @@> (_, _, [a; b]) ->
            Operator (LesserOrEqualTo (parse state a, parse state b))
        | DP.SpecificCall <@@ not @@> (_, _, [a]) ->
            Not (parse state a)
        | DP.SpecificCall <@@ between @@> (_, _, exprs) ->
            match exprs |> List.map (parse state) with
            | [ (FieldName _ as x); (Value (SqlValue _) as a); (Value(SqlValue _) as b) ] ->
                Between (x, a, b)
            | _ -> 
                raise <| InvalidQueryException "Invalid 'between'"
        | DP.SpecificCall <@@ like @@> (_, _, exprs) ->
            match exprs |> List.map (parse state) with
            | [ (FieldName _ as x); (Value(SqlValue (Text _)) as s) ] ->
                Like (x, s)
            | _ ->
                raise <| InvalidQueryException "Invalid 'like'"
        | DP.SpecificCall <@@ isIn @@> (_, _, exprs) ->
            match exprs |> List.map (parse state) with
            | [ (FieldName _ as x); Value(SqlValues _) as xs ] ->
                In (x, xs)
            | _ ->
                raise <| InvalidQueryException "Invalid 'isIn'"
        // --------------------------------------------------------------------
        | P.Let (var, assignment, body) ->
            match parse state assignment with
            | Value value ->
                let state = { state with Env = state.Env |> Map.add var.Name value }
                parse state body
            | _ ->
                raise <| InvalidQueryException "Let assignment was not a value"
        | P.Var var ->
            state.Env 
            |> Map.find var.Name
            |> Value
        | P.Lambda (var, expr) ->
            match state.Subject with
            | None -> parse { state with Subject = Some var.Name } expr
            | Some subject ->
                raise <| InvalidQueryException "Only 1 lambda is supported, the one used to do the query"
        | DP.AndAlso (a, b) ->
            And (parse state a, parse state b)
        | DP.OrElse (a, b) ->
            Or (parse state a, parse state b)
        | P.FieldGet (var, info) ->
            match info.DeclaringType with
            | t when t = typeof<DateTime> && info.Name = "MinValue" ->
                Value (SqlValue <| SqlValue.FromValue DateTime.MinValue)
            | t when t = typeof<DateTime> && info.Name = "MaxValue" ->
                Value (SqlValue <| SqlValue.FromValue DateTime.MaxValue)
            | t when t = typeof<Guid> && info.Name = "Empty" ->
                Value (SqlValue <| SqlValue.FromValue Guid.Empty)
            | _ -> 
                FieldName info.Name
        | P.PropertyGet (o_expr, info, exprs) ->
            let o_subject = state.Subject
            let o_fieldName =
                let rec loop state o_expr =
                    match o_expr, o_subject with
                    | Some (P.PropertyGet (o_expr, info, _)), Some subject -> 
                        match state with
                        | None -> loop (Some info.Name) o_expr
                        | Some state -> loop (Some (state + "_" + info.Name)) o_expr
                    | Some (P.Var var), Some subject when var.Name = subject -> 
                        match state with
                        | None -> Some info.Name
                        | Some state -> Some (state + "_" + info.Name)
                    | _, _ -> None
                loop None o_expr
            match o_fieldName with
            | Some fieldName ->
                FieldName fieldName
            | None ->
                let value = eval expr
                Value (SqlValue <| SqlValue.FromValue value)
        | P.Value _ ->
            let value = eval expr
            Value (SqlValue <| SqlValue.FromValue value)
        | P.NewUnionCase (case, exprList) ->
            match exprList with
            | [ single ] when case.DeclaringType = typeof<PrimaryKey<_>> ->
                parse state single
            | [ single ] when case.DeclaringType = typeof<ForeignKey<_>> ->
                parse state single
            | _ ->
                let values = 
                    exprList
                    |> List.map (fun expr ->
                        match parse state expr with
                        | Value value -> value
                        | _ -> raise <| InvalidQueryException "NewUnionCase expr wasn't a value"
                    )
                    |> Array.ofList
                Value (SqlUnion { TagName = case.Name; TagIndex = case.Tag; Values = values })
        | P.NewRecord (t, exprList) ->
            let fields = FSharpType.GetRecordFields t
            let values = 
                exprList 
                |> List.map (fun expr ->
                    match parse state expr with
                    | Value value -> value
                    | _ -> raise <| InvalidQueryException "NewRecord expr wasn't a value"
                )
                |> Array.ofList
            (fields, values)
            ||> Array.zip
            |> Array.map (fun (prop, value) ->
                prop.Name, value
            )
            |> Map.ofArray
            |> SqlRecord
            |> Value
        | P.Call _ ->
            let value = eval expr
            Value (SqlValue <| SqlValue.FromValue value)
        | P.NewArray (_, xs) ->
            let sqlValues = 
                xs 
                |> List.choose (fun e ->
                    match parse state e with
                    | Value (SqlValue value) -> Some value
                    | _ -> None
                )
            if List.length xs = List.length sqlValues then
                sqlValues
                |> Array.ofList
                |> SqlValues 
                |> Value
            else
                raise <| InvalidQueryException "Could not extract all the values of the array"
        | P.NewTuple exprList ->
            let sqlValues =
                exprList
                |> List.choose (fun e ->
                    match parse state e with
                    | Value value -> Some value
                    | _ -> None
                )
            if List.length exprList = List.length sqlValues then
                sqlValues
                |> Array.ofList
                |> SqlTuple
                |> Value
            else
                raise <| InvalidQueryException "Could not extract all the values of the tuple"
        | _ -> 
            raise <| InvalidQueryException (sprintf "Invalid query expression: %O" expr)

let parse (expr: Quotations.Expr<'t -> bool>) =
    let queryExpr = Private.parse Private.State.New expr
    Private.queryExprString queryExpr
